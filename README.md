# vue-core
Пример приложения на основе [Vue CLI 3](https://cli.vuejs.org/guide/).
## Установка заивсимостей
```
npm install
```

### Запуск приложения локально в дев режиме
```
npm run serve
```
### Запускает сборку dev(stage) бандла в папку dist
```
npm run stage-build
```

### Запускает сборку продакшн бандла в папку dist
```
npm run build
```

### Запуск тестов
```
npm run test
```

### Запуск линтера
```
npm run lint
```

### Запуск юнит тестов
```
npm run test:unit
```

## Структура папок и файлов
```
├── public/                     # Публичные файлы
├── src/                        # Исходники
│   ├── api/                    # Общее для всего приложения апи
│   │   └── core.ts/js          # Сущность аксиоса от которой наследуются апи сервисов приложения
│   ├── assets/                 # Статичные файлы приложения (картинки, e.t.c.)
│   ├── components/             # Общие для приложения компоненты
│   ├── store/                  # Общий стор приложения (vuex)
│   │   ├── rootStore.ts/js     # Основной стор для импорта модулей каждого отдельного сервиса
│   │   └── *modules/           # Общие для приложения модули
│   ├── utils/                  # Утилиты
│   ├── plugins/                # Плагины
│   ├── views/                  # Сервисы/страницы
│   │   └── demoPage/           # Тестовый сервис/страница
│   │       ├── *api/           # Апи конкретного сервиса/страницы
│   │       ├── *store/         # Модуль хранилища конкретного сервиса/страницы
│   │       ├── *components/    # Компоненты конкретного сервиса/страницы
│   │       └── index.vue       # Основной контейнер сервиса/страницы
│   ├── App.vue                 # Основной контейнер приложения
│   ├── main.ts/js              # Точка входа в приложение
│   ├── router.ts/js            # Роутер приложения
│   ├── shims-tsx.d.ts          # Файл объявления ts
│   └── shims-vue.d.ts          # Файл объявления ts
├── dist/                       # Сборка (автогенерация)
├── tests/                      # Тесты
├── .dockerignore               # Список исключений для Docker
├── .editorconfig               # Конфигурация настроек редактора кода
├── .env                        # Переменная окружения продакшен, пример использования в api/core.ts
├── .env.development            # Переменная окружения дев, пример использования в api/core.ts
├── .env.staging                # Переменная окружения стейдж билда, пример использования в api/core.ts
├── .gitignore                  # Список исключений для Git
├── .gitlab-ci.yml              # Описание пайплайна гитлаба
├── babel.config.js             # Настройки бабеля
├── Dockerfile                  # Исполняемый файл для Docker
├── nginx.conf                  # Настройки nginx
├── package.json                # Список модулей и прочей информации, настройки почти всех плагинов приложения
├── package-lock.json           # Список зависимостей сборок разных версий
├── tsconfig.json               # Настройки typescript
├── README.md                   # Документация шаблона
├── Version                     # Версия
└──  Kubernetes.yaml            # Настройки деплоя в kubernetes
```
### Подробности конфигурации
[Vue](https://vuejs.org/)<br>
[Vuex](https://vuex.vuejs.org/ru/guide/)<br>
[Configuration Reference](https://cli.vuejs.org/config/)<br>
[Axios](https://github.com/axios/axios)<br>
[TypeScript](https://www.typescriptlang.org/docs/home.html)<br>
[Lodash](https://lodash.com/)<br>
[ESLint](https://eslint.org/) (конфигурация располагается в package.json)<br>
[Airbnb](https://github.com/airbnb/javascript) (основной плагин линтера)<br>
[Notifications](https://github.com/euvl/vue-notification)<br>
[k8s](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)<br>
[Docker](https://www.docker.com/)<br>
[Vuetify](https://vuetifyjs.com/en/) - в сборку не установлен (```vue add vuetify```), но пользуемся в основном им
