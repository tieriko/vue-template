export const randomNumber = {
  inRangeAb: (min: number, max: number) => {
    return Math.random() * (max - min) + min;
  },
  getRandomInt: (min: number, max: number) => {
      return Math.floor(Math.random() * (max - min + 1)) + min;
  },
};
