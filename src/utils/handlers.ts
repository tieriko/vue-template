import Vue from 'vue';
import _ from 'lodash';

export const handlers = {
  defaultErrorHandler: (err: string) => {
    console.log(err);
    Vue.notify({
      group: 'handler',
      title: 'Произошла ошибка',
      text: err,
      type: 'error',
      duration: 3500,
    });
    // throw new Error(err);
  },
  defaultErrorAPIHandler: (errorText: string, status: string) => {
    Vue.notify({
      group: 'handler',
      title: `Произошла ошибка: ${status}`,
      text: errorText,
      type: 'error',
      duration: 3500,
    });
    // throw new Error(errorText)
  },
  defaultErrorResponseHandler: (error: any, onError: any) => {
    const msg = `${error.message
    }. ${
      !_.isUndefined(error.response) ? error.response.data : ''}`;
    if (_.isFunction(onError)) {
      onError(msg);
    } else {
      handlers.defaultErrorHandler(msg);
    }
  },
  defaultSuccessHandler: (title: string, message: string) => {
    Vue.notify({
      group: 'handler',
      title,
      text: message,
      type: 'success',
      duration: 3500,
    });
  },
  defaultWarningHandler: (title: string, message: string) => {
    Vue.notify({
      group: 'handler',
      title,
      text: message,
      type: 'warn',
      duration: 3500,
    });
  },
  pageErrorHandler: (title: string, message: string) => {
    Vue.notify({
      group: 'handler',
      title,
      text: message,
      type: 'error',
      duration: 3500,
    });
  },
};
