import { AxiosRequestConfig } from 'axios';
import { ServiceBase, Url } from '@/api/core';
import { IResponseErrorHandler, IResponseSuccessHandler } from '@/api/types';

const aboutAppApiGet: AxiosRequestConfig = {
  method: 'GET',
  baseURL: `${Url}/api/directory/`,
};


export class DemoService extends ServiceBase {
  public static getSubjects(
    onSuccess: IResponseSuccessHandler,
    onError?: IResponseErrorHandler,
  ) {
    return ServiceBase.callApi(aboutAppApiGet, '/subjects/', onSuccess, onError);
  }

  public static getAddedOption(
    onSuccess: IResponseSuccessHandler,
    onError?: IResponseErrorHandler,
  ) {
    return ServiceBase.callApi(aboutAppApiGet, '/addedoptions/', onSuccess, onError);
  }
}
