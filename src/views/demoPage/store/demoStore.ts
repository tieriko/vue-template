import {
  MutationTree, GetterTree, Module, ActionTree,
} from 'vuex';
import { IAddedOption, ISubject, DemoState } from './types';
import { RootState } from '@/store/types';
import { DemoService } from '../api/demoService';
import { handlers } from '@/utils/handlers';

const state: DemoState = {
  subjects: new Map(), // Мапа может использоваться только в тех случаях,
  subjectsIDs: [], // когда данные остаются иммутабельными,
  addedOptions: new Map(), // т.к. данные в ней не являются реактивными.
  addedOptionsIDs: [],
};

const getters: GetterTree<DemoState, RootState> = {
  Subjects: state => state.subjects,
  SubjectsMap: state => state.subjectsIDs,
  AOS: demoState => demoState.addedOptions,
  AOSMap: demoState => demoState.addedOptionsIDs,
};

const mutations: MutationTree<DemoState> = {
  SET_SUBJECTS(state, payload: ISubject[]): void {
    payload.forEach((subj) => {
      state.subjects.set(subj.id, subj);
      state.subjectsIDs.push(subj.id);
    });
    handlers.defaultSuccessHandler('Успех!', 'Загрузка предметов завершена');
  },
  SET_AOS(demoState, payload: IAddedOption[]): void {
    payload.forEach((ao) => {
      demoState.addedOptions.set(ao.id, ao);
      demoState.addedOptionsIDs.push(ao.id);
    });
    handlers.defaultSuccessHandler('Успех!', 'Загрузка доп. свойств завершена');
  },
};

const actions: ActionTree<DemoState, RootState> = {
  async getSubjects({ commit }) {
    await DemoService.getSubjects((response: ISubject[]) => {
      commit('SET_SUBJECTS', response);
    });
  },
  async getAddedOptions({ commit }) {
    await DemoService.getAddedOption((response: IAddedOption[]) => {
      commit('SET_AOS', response);
    });
  },

};
const namespaced: boolean = true;

export const demoState: Module<DemoState, RootState> = {
  namespaced,
  state,
  getters,
  mutations,
  actions,
};
