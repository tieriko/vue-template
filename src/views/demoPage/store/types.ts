export interface DemoState {
  subjects: Map<number, ISubject>,
    subjectsIDs: number[],
    addedOptions: Map<number, IAddedOption>,
    addedOptionsIDs: number[],
}

export interface ISubject {
  parentid: number | null,
  id: number,
  name: string
}

export interface IAddedOption {
  requiredmode: number,
  si: null | Isi;
  typeid: number;
  id: number;
  name: string;
}

export interface Isi {
  id: number;
  name: string;
}
