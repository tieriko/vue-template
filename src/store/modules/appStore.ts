import { GetterTree, Module } from 'vuex';
import { AppState, Page } from './types';
import { RootState } from '@/store/types';

export const state: AppState = {
  pages: [
    {
      title: 'Home',
      href: '/',
    },
    {
      title: 'About',
      href: '/about',
    },
    {
      title: 'Demo',
      href: '/demo-page',
    },

  ],
};
const getters: GetterTree<AppState, RootState> = {
  Pages: (appState): Page[] => appState.pages,
};

const namespaced: boolean = true;

export const appState: Module<AppState, RootState> = {
  namespaced,
  state,
  getters,
};
