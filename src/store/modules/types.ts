export interface AppState {
  pages: Page[],
}

export interface Page {
  title: string,
  href: string,
}
