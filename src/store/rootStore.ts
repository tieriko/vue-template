import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { RootState } from './types';
import { appState } from './modules/appStore';
import { demoState } from '@/views/demoPage/store/demoStore';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  modules: {
    appState,
    demoState,
  },

};

export default new Vuex.Store<RootState>(store);
